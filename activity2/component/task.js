import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { CheckBox, TouchableOpacity } from "react-native-web";

const Task = (props) => {
    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <View style={styles.circle}></View>
                <Text style={styles.itemText}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#FFA07A',
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingVertical: 50,
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    circle: {
        width: 27,
        height: 27,
        backgroundColor: '#000',
        borderWidth: 3,
        borderColor: '#FFA07A',
        borderRadius: 60,
        marginRight: 15,
    },
    itemText: {
        maxWidth: '85%',
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
    },
});
export default Task;