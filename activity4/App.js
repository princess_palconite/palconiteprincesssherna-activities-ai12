import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// Screens
import AppIntro from './src/screens/AppIntro';

const Stack = createNativeStackNavigator();

export default function App () {

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='App Intro' screenOptions={{headerShown: false}}>
          <Stack.Screen name="App Intro" component={AppIntro} />
        </Stack.Navigator>
    </NavigationContainer>
  )
};