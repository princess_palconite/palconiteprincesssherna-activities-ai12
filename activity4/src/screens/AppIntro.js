import React, {useState} from 'react';
import { StyleSheet, Text, Image, View} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
 
const slides = [
  {
    key: '1',
    title: 'slide 1',
    subtitle: ' Its enjoyable and interesting to study because you are involved in digital work',
    image: require('../images/1.png'),
  },
  {
    key: '2',
    title: 'slide 2',
    subtitle: 'It is one of the fastest growing industries, with a wide range of career opportunities. Aside from quick employment and higher pay, the IT industry has provided career paths for every industry as well as multiple career paths.',
    image: require('../images/2.png'),
  },
  {
    key: '3',
    title: 'slide 3',
    subtitle: 'I have choose information technology since it focuses on the use of computers and related software to manage information systems in able to serve the needs of industry and the government.',
    image: require('../images/3.png'),
  },
  {
    key: '4',
    title: 'slide  4',
    subtitle: 'I would like to focus on information technology knowledge such as communications, computer systems, software systems, database management, application development, web design, digital media, and electronic publishing.',
    image: require('../images/4.png'),
  },
  {
    key: '5',
    title: 'slide 5',
    subtitle: 'I chose BSIT because it can provide a thorough understanding of the most recent information technology and its applications in the business world.',
    image: require('../images/5.png'),
  },
];

const Slide = ({ item }) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <Image 
            source={item.image}
            style={{height: '75%', width: '100%', resizeMode: 'contain'}}
        />
        <Text style={styles.subtitle}>{item.subtitle}</Text>
      </View>
    );
};

export default function AppIntro ({navigation}) {
    return (
        <AppIntroSlider
          data={slides}
          renderItem={({item}) => <Slide item={item}/>}
          onDone={()=> navigation.push('App Intro')}
          activeDotStyle={{
            backgroundColor:"#21465b",
            width:10
          }}
          showDoneButton={true}
          renderDoneButton={()=><Text style={{color: 'black', margin: 20, fontSize: 18, fontWeight: 'bold'}}>Home</Text>}
          showNextButton={true}
          renderNextButton={()=><Text style={{color: 'black', margin: 20, fontSize: 18, fontWeight: 'bold'}}>Continue</Text>}
        />
    );
}

const styles = StyleSheet.create({
    slide: {
        alignItems: 'center',
        backgroundColor: 'white',
    },
    subtitle: {
        color: 'orange',
        fontSize: 15,
        marginHorizontal: 20,
        maxWidth: '120%',
        textAlign: 'justify',
        lineHeight: 25,
    },
    title: {
        color: 'orange',
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 45,
        textAlign: 'center',
    },
})